from django import forms
from .models import dtbs_motor



class MotorForm(forms.ModelForm):
    class Meta:
        model = dtbs_motor
        fields = ['category']
        labels = {
            'category' : 'category'
        }


