from django.urls import path
from . import views

app_name = 'dtbsmotor'

urlpatterns = [
    path('', views.dtbsmotor, name='dtbsmotor'),   
    # path('', views.showdata, name='showdata')
]