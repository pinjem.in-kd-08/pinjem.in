from django.db import models

# Create your models here.
JENIS_MOTOR=(
    ('a','Automatic'),
    ('b','Manual')
)


class dtbs_motor(models.Model):
    picture = models.ImageField(null=True, blank=True,upload_to = 'dtbsmotor/static/photo')
    motorcycle_type = models.CharField(max_length=100)
    price = models.CharField(max_length=100)
    location = models.CharField(max_length=100)
    category = models.CharField(max_length=1,choices=JENIS_MOTOR)

    def __str__(self):
        return self.motorcycle_type

# class buat_filter(models.Model):
#     kategori = models.CharField(max_length = 1, choices = JENIS_MOTOR)