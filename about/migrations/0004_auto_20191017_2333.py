# Generated by Django 2.2.6 on 2019-10-17 16:33

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('about', '0003_auto_20191017_2328'),
    ]

    operations = [
        migrations.AlterField(
            model_name='add_feedback',
            name='time',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
