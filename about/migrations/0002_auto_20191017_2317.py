# Generated by Django 2.2.6 on 2019-10-17 16:17

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('about', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='add_feedback',
            name='time',
            field=models.DateField(default=django.utils.timezone.now),
        ),
    ]
