from django.db import models

class Snippet(models.Model):
    name = models.CharField(max_length =40)
    contact = models.CharField(max_length =40)
    email =  models.EmailField()
    tipe = models.CharField(max_length =40)
    condition = models.CharField(max_length =100)

    def __str__(self):
        return self.name
