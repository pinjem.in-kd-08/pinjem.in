from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import ContactForm

# Create your views here.

def contact(request):

    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            # name = form.cleaned_data['name']
            # email =  form.cleaned_data['email']
            # tipe = form.cleaned_data['tipe']
            # condition = form.cleaned_data['condition']
            form.save()
            return redirect('/')
    else:
        form = ContactForm()
    return render(request, 'form.html', {'form': form})



