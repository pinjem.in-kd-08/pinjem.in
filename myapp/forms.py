from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit
from django.core.validators import RegexValidator
from .models import Snippet

class ContactForm(forms.ModelForm):
    class Meta:
        model = Snippet
        fields = ('name', 'contact', 'email', 'tipe', 'condition')
        
    
    name = forms.CharField(label = 'Name', validators=[RegexValidator(r'[a-zA-z]+', 'Please enter a valid name!')])
    contact = forms.CharField(label = 'Contact/Phone', validators=[RegexValidator(r'[0-9]+', 'Please enter a valid number!')])
    email =  forms.EmailField(label = 'E-Mail')
    tipe = forms.CharField(label = 'Type of Motorcycle')
    condition = forms.CharField(label = 'Condition')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper
        self.helper.form_method = 'post'

        self.helper.layout = Layout(
            'name',
            'contact',
            'email',
            'tipe',
            'condition',
            
            Submit('submit', 'Pinjemin Now', css_class = 'btn-dark')
        )


