from django.db import models
from django.utils import timezone
from datetime import datetime, date

class Pemesanan(models.Model):
    pickuptime = models.DateTimeField()
    returningtime = models.DateTimeField()
    locationpickup = models.CharField(max_length=40)
    locationreturn = models.CharField(max_length=40)
    name = models.CharField(max_length=40)
    phonenumber = models.CharField(max_length=14)
    email = models.CharField(max_length=30)
    additionalinfo = models.CharField(max_length=200)

# Create your models here.
