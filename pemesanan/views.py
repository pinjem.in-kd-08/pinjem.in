from django.shortcuts import render
from .models import Pemesanan

def form_pemesanan(request):
	return render(request, 'pemesanan.html')


def add_pemesanan(request):
	if (request.method == 'POST'):
		pickuptime = request.POST['pickuptime']
		returningtime = request.POST['returningtime']
		locationpickup = request.POST['locationpickup']
		locationreturn = request.POST['locationreturn']
		name = request.POST['name']
		phonenumber = request.POST['phonenumber']
		email = request.POST['email']
		additionalinfo = request.POST['additionalinfo']
		pemesanan = Pemesanan(pickuptime = pickuptime, returningtime = returningtime, locationpickup = locationpickup, locationreturn = locationreturn, name = name, email = email, phonenumber = phonenumber, additionalinfo = additionalinfo)
		pemesanan.save()
		return render(request, 'feedback-pemesanan.html')


# Create your views here.
